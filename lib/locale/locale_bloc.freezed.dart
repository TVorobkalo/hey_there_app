// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'locale_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$LocaleEventTearOff {
  const _$LocaleEventTearOff();

// ignore: unused_element
  _LoadLocales loadLocales(BuildContext context) {
    return _LoadLocales(
      context,
    );
  }

// ignore: unused_element
  _SelectLangugage selectLanguage(Language language) {
    return _SelectLangugage(
      language,
    );
  }
}

// ignore: unused_element
const $LocaleEvent = _$LocaleEventTearOff();

mixin _$LocaleEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadLocales(BuildContext context),
    @required Result selectLanguage(Language language),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadLocales(BuildContext context),
    Result selectLanguage(Language language),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadLocales(_LoadLocales value),
    @required Result selectLanguage(_SelectLangugage value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadLocales(_LoadLocales value),
    Result selectLanguage(_SelectLangugage value),
    @required Result orElse(),
  });
}

abstract class $LocaleEventCopyWith<$Res> {
  factory $LocaleEventCopyWith(
          LocaleEvent value, $Res Function(LocaleEvent) then) =
      _$LocaleEventCopyWithImpl<$Res>;
}

class _$LocaleEventCopyWithImpl<$Res> implements $LocaleEventCopyWith<$Res> {
  _$LocaleEventCopyWithImpl(this._value, this._then);

  final LocaleEvent _value;
  // ignore: unused_field
  final $Res Function(LocaleEvent) _then;
}

abstract class _$LoadLocalesCopyWith<$Res> {
  factory _$LoadLocalesCopyWith(
          _LoadLocales value, $Res Function(_LoadLocales) then) =
      __$LoadLocalesCopyWithImpl<$Res>;
  $Res call({BuildContext context});
}

class __$LoadLocalesCopyWithImpl<$Res> extends _$LocaleEventCopyWithImpl<$Res>
    implements _$LoadLocalesCopyWith<$Res> {
  __$LoadLocalesCopyWithImpl(
      _LoadLocales _value, $Res Function(_LoadLocales) _then)
      : super(_value, (v) => _then(v as _LoadLocales));

  @override
  _LoadLocales get _value => super._value as _LoadLocales;

  @override
  $Res call({
    Object context = freezed,
  }) {
    return _then(_LoadLocales(
      context == freezed ? _value.context : context as BuildContext,
    ));
  }
}

class _$_LoadLocales with DiagnosticableTreeMixin implements _LoadLocales {
  const _$_LoadLocales(this.context) : assert(context != null);

  @override
  final BuildContext context;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LocaleEvent.loadLocales(context: $context)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LocaleEvent.loadLocales'))
      ..add(DiagnosticsProperty('context', context));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _LoadLocales &&
            (identical(other.context, context) ||
                const DeepCollectionEquality().equals(other.context, context)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(context);

  @override
  _$LoadLocalesCopyWith<_LoadLocales> get copyWith =>
      __$LoadLocalesCopyWithImpl<_LoadLocales>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadLocales(BuildContext context),
    @required Result selectLanguage(Language language),
  }) {
    assert(loadLocales != null);
    assert(selectLanguage != null);
    return loadLocales(context);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadLocales(BuildContext context),
    Result selectLanguage(Language language),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadLocales != null) {
      return loadLocales(context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadLocales(_LoadLocales value),
    @required Result selectLanguage(_SelectLangugage value),
  }) {
    assert(loadLocales != null);
    assert(selectLanguage != null);
    return loadLocales(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadLocales(_LoadLocales value),
    Result selectLanguage(_SelectLangugage value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loadLocales != null) {
      return loadLocales(this);
    }
    return orElse();
  }
}

abstract class _LoadLocales implements LocaleEvent {
  const factory _LoadLocales(BuildContext context) = _$_LoadLocales;

  BuildContext get context;
  _$LoadLocalesCopyWith<_LoadLocales> get copyWith;
}

abstract class _$SelectLangugageCopyWith<$Res> {
  factory _$SelectLangugageCopyWith(
          _SelectLangugage value, $Res Function(_SelectLangugage) then) =
      __$SelectLangugageCopyWithImpl<$Res>;
  $Res call({Language language});
}

class __$SelectLangugageCopyWithImpl<$Res>
    extends _$LocaleEventCopyWithImpl<$Res>
    implements _$SelectLangugageCopyWith<$Res> {
  __$SelectLangugageCopyWithImpl(
      _SelectLangugage _value, $Res Function(_SelectLangugage) _then)
      : super(_value, (v) => _then(v as _SelectLangugage));

  @override
  _SelectLangugage get _value => super._value as _SelectLangugage;

  @override
  $Res call({
    Object language = freezed,
  }) {
    return _then(_SelectLangugage(
      language == freezed ? _value.language : language as Language,
    ));
  }
}

class _$_SelectLangugage
    with DiagnosticableTreeMixin
    implements _SelectLangugage {
  const _$_SelectLangugage(this.language) : assert(language != null);

  @override
  final Language language;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LocaleEvent.selectLanguage(language: $language)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LocaleEvent.selectLanguage'))
      ..add(DiagnosticsProperty('language', language));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SelectLangugage &&
            (identical(other.language, language) ||
                const DeepCollectionEquality()
                    .equals(other.language, language)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(language);

  @override
  _$SelectLangugageCopyWith<_SelectLangugage> get copyWith =>
      __$SelectLangugageCopyWithImpl<_SelectLangugage>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loadLocales(BuildContext context),
    @required Result selectLanguage(Language language),
  }) {
    assert(loadLocales != null);
    assert(selectLanguage != null);
    return selectLanguage(language);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loadLocales(BuildContext context),
    Result selectLanguage(Language language),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectLanguage != null) {
      return selectLanguage(language);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loadLocales(_LoadLocales value),
    @required Result selectLanguage(_SelectLangugage value),
  }) {
    assert(loadLocales != null);
    assert(selectLanguage != null);
    return selectLanguage(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loadLocales(_LoadLocales value),
    Result selectLanguage(_SelectLangugage value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (selectLanguage != null) {
      return selectLanguage(this);
    }
    return orElse();
  }
}

abstract class _SelectLangugage implements LocaleEvent {
  const factory _SelectLangugage(Language language) = _$_SelectLangugage;

  Language get language;
  _$SelectLangugageCopyWith<_SelectLangugage> get copyWith;
}

class _$LocaleStateTearOff {
  const _$LocaleStateTearOff();

// ignore: unused_element
  _NewLocale newLocale(@nullable Locale locale) {
    return _NewLocale(
      locale,
    );
  }
}

// ignore: unused_element
const $LocaleState = _$LocaleStateTearOff();

mixin _$LocaleState {
  @nullable
  Locale get locale;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result newLocale(@nullable Locale locale),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result newLocale(@nullable Locale locale),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result newLocale(_NewLocale value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result newLocale(_NewLocale value),
    @required Result orElse(),
  });

  $LocaleStateCopyWith<LocaleState> get copyWith;
}

abstract class $LocaleStateCopyWith<$Res> {
  factory $LocaleStateCopyWith(
          LocaleState value, $Res Function(LocaleState) then) =
      _$LocaleStateCopyWithImpl<$Res>;
  $Res call({@nullable Locale locale});
}

class _$LocaleStateCopyWithImpl<$Res> implements $LocaleStateCopyWith<$Res> {
  _$LocaleStateCopyWithImpl(this._value, this._then);

  final LocaleState _value;
  // ignore: unused_field
  final $Res Function(LocaleState) _then;

  @override
  $Res call({
    Object locale = freezed,
  }) {
    return _then(_value.copyWith(
      locale: locale == freezed ? _value.locale : locale as Locale,
    ));
  }
}

abstract class _$NewLocaleCopyWith<$Res> implements $LocaleStateCopyWith<$Res> {
  factory _$NewLocaleCopyWith(
          _NewLocale value, $Res Function(_NewLocale) then) =
      __$NewLocaleCopyWithImpl<$Res>;
  @override
  $Res call({@nullable Locale locale});
}

class __$NewLocaleCopyWithImpl<$Res> extends _$LocaleStateCopyWithImpl<$Res>
    implements _$NewLocaleCopyWith<$Res> {
  __$NewLocaleCopyWithImpl(_NewLocale _value, $Res Function(_NewLocale) _then)
      : super(_value, (v) => _then(v as _NewLocale));

  @override
  _NewLocale get _value => super._value as _NewLocale;

  @override
  $Res call({
    Object locale = freezed,
  }) {
    return _then(_NewLocale(
      locale == freezed ? _value.locale : locale as Locale,
    ));
  }
}

class _$_NewLocale with DiagnosticableTreeMixin implements _NewLocale {
  const _$_NewLocale(@nullable this.locale);

  @override
  @nullable
  final Locale locale;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'LocaleState.newLocale(locale: $locale)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'LocaleState.newLocale'))
      ..add(DiagnosticsProperty('locale', locale));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NewLocale &&
            (identical(other.locale, locale) ||
                const DeepCollectionEquality().equals(other.locale, locale)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(locale);

  @override
  _$NewLocaleCopyWith<_NewLocale> get copyWith =>
      __$NewLocaleCopyWithImpl<_NewLocale>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result newLocale(@nullable Locale locale),
  }) {
    assert(newLocale != null);
    return newLocale(locale);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result newLocale(@nullable Locale locale),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (newLocale != null) {
      return newLocale(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result newLocale(_NewLocale value),
  }) {
    assert(newLocale != null);
    return newLocale(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result newLocale(_NewLocale value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (newLocale != null) {
      return newLocale(this);
    }
    return orElse();
  }
}

abstract class _NewLocale implements LocaleState {
  const factory _NewLocale(@nullable Locale locale) = _$_NewLocale;

  @override
  @nullable
  Locale get locale;
  @override
  _$NewLocaleCopyWith<_NewLocale> get copyWith;
}
