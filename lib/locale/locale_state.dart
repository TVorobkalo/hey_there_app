part of 'locale_bloc.dart';

@freezed
abstract class LocaleState with _$LocaleState {
  const factory LocaleState.newLocale(@nullable Locale locale) = _NewLocale;
}