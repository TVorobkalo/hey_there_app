part of 'locale_bloc.dart';

@freezed
abstract class LocaleEvent with _$LocaleEvent {
  const factory LocaleEvent.loadLocales(BuildContext context) =
  _LoadLocales;

  const factory LocaleEvent.selectLanguage(
      Language language,
      ) = _SelectLangugage;
}
