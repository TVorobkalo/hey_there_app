import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hey_there_app/injection.dart';
import 'package:hey_there_app/locale/locale_service.dart';
import 'package:hey_there_app/domain/core/value_failures.dart';
import 'package:injectable/injectable.dart';

part 'locale_bloc.freezed.dart';
part 'locale_event.dart';
part 'locale_state.dart';

@lazySingleton
class LocaleBloc extends Bloc<LocaleEvent, LocaleState> {
  LocaleBloc() : super(const LocaleState.newLocale(null));

  @override
  Stream<LocaleState> mapEventToState(LocaleEvent event) async* {
    yield* event.when(
      loadLocales: _mapLoadLocaleToState,
      selectLanguage: _mapSelectLanguageToState,
    );
  }

  Stream<LocaleState> _mapLoadLocaleToState(BuildContext context) async* {
    final localeService = await getIt.getAsync<LocaleService>();
    final localeOption = localeService.savedLocale;

    yield LocaleState.newLocale(
        localeOption.getOrElse(() => Localizations.localeOf(context)));
  }

  Stream<LocaleState> _mapSelectLanguageToState(
    Language newLanguage,
  ) async* {
    final localeService = await getIt.getAsync<LocaleService>();

    await localeService.setLanguage(newLanguage);
    yield LocaleState.newLocale(localeService.savedLocale.getOrElse(() =>
        throw UnexpectedValueError(ValueFailure.languageIsNull(
            failedValue: localeService.savedLocale))));
  }
}

class UnexpectedValueError extends Error {
  final ValueFailure valueFailure;

  UnexpectedValueError(this.valueFailure);

  @override
  String toString() {
    const explanation =
        'Encountered a ValueFailure at an unrecoverable point. Terminating.';
    return Error.safeToString('$explanation Failure was: $valueFailure');
  }
}

enum Language { en, uk }
