import 'dart:ui' show Locale;

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:hey_there_app/locale/locale_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@singleton
class LocaleService {
  static const langCode = 'languageCode';

  static SharedPreferences _preferences;

  LocaleService._internal();

  @factoryMethod
  static Future<LocaleService> create() async {
    _preferences ??= await SharedPreferences.getInstance();
    return LocaleService._internal();
  }

  Future<bool> setLanguage(Language language) =>
      _preferences.setString(langCode, describeEnum(language));

  String get _languageCode {
    final string = _preferences.getString(langCode);
    debugPrint('Locale in preferences: $string');
    return string;
  }

  Option<Locale> get savedLocale {
    if (_languageCode == 'en') {
      return some(const Locale('en', 'US'));
    } else if (_languageCode == 'uk') {
      return some(const Locale('uk', 'UK'));
    }
    return none();
  }
}
