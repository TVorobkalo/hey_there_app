import 'package:flutter/material.dart';
import 'package:hey_there_app/presentation/app_widget.dart';
import 'package:hey_there_app/injection.dart';
import 'package:injectable/injectable.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  configureInjection(Environment.prod);
  runApp(AppWidget());
}
