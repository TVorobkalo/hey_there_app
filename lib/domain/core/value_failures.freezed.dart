// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'value_failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$ValueFailureTearOff {
  const _$ValueFailureTearOff();

// ignore: unused_element
  LanguageIsNull<T> languageIsNull<T>({@required T failedValue}) {
    return LanguageIsNull<T>(
      failedValue: failedValue,
    );
  }
}

// ignore: unused_element
const $ValueFailure = _$ValueFailureTearOff();

mixin _$ValueFailure<T> {
  T get failedValue;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result languageIsNull(T failedValue),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result languageIsNull(T failedValue),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result languageIsNull(LanguageIsNull<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result languageIsNull(LanguageIsNull<T> value),
    @required Result orElse(),
  });

  $ValueFailureCopyWith<T, ValueFailure<T>> get copyWith;
}

abstract class $ValueFailureCopyWith<T, $Res> {
  factory $ValueFailureCopyWith(
          ValueFailure<T> value, $Res Function(ValueFailure<T>) then) =
      _$ValueFailureCopyWithImpl<T, $Res>;
  $Res call({T failedValue});
}

class _$ValueFailureCopyWithImpl<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  _$ValueFailureCopyWithImpl(this._value, this._then);

  final ValueFailure<T> _value;
  // ignore: unused_field
  final $Res Function(ValueFailure<T>) _then;

  @override
  $Res call({
    Object failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue:
          failedValue == freezed ? _value.failedValue : failedValue as T,
    ));
  }
}

abstract class $LanguageIsNullCopyWith<T, $Res>
    implements $ValueFailureCopyWith<T, $Res> {
  factory $LanguageIsNullCopyWith(
          LanguageIsNull<T> value, $Res Function(LanguageIsNull<T>) then) =
      _$LanguageIsNullCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

class _$LanguageIsNullCopyWithImpl<T, $Res>
    extends _$ValueFailureCopyWithImpl<T, $Res>
    implements $LanguageIsNullCopyWith<T, $Res> {
  _$LanguageIsNullCopyWithImpl(
      LanguageIsNull<T> _value, $Res Function(LanguageIsNull<T>) _then)
      : super(_value, (v) => _then(v as LanguageIsNull<T>));

  @override
  LanguageIsNull<T> get _value => super._value as LanguageIsNull<T>;

  @override
  $Res call({
    Object failedValue = freezed,
  }) {
    return _then(LanguageIsNull<T>(
      failedValue:
          failedValue == freezed ? _value.failedValue : failedValue as T,
    ));
  }
}

class _$LanguageIsNull<T>
    with DiagnosticableTreeMixin
    implements LanguageIsNull<T> {
  const _$LanguageIsNull({@required this.failedValue})
      : assert(failedValue != null);

  @override
  final T failedValue;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ValueFailure<$T>.languageIsNull(failedValue: $failedValue)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ValueFailure<$T>.languageIsNull'))
      ..add(DiagnosticsProperty('failedValue', failedValue));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is LanguageIsNull<T> &&
            (identical(other.failedValue, failedValue) ||
                const DeepCollectionEquality()
                    .equals(other.failedValue, failedValue)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failedValue);

  @override
  $LanguageIsNullCopyWith<T, LanguageIsNull<T>> get copyWith =>
      _$LanguageIsNullCopyWithImpl<T, LanguageIsNull<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result languageIsNull(T failedValue),
  }) {
    assert(languageIsNull != null);
    return languageIsNull(failedValue);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result languageIsNull(T failedValue),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (languageIsNull != null) {
      return languageIsNull(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result languageIsNull(LanguageIsNull<T> value),
  }) {
    assert(languageIsNull != null);
    return languageIsNull(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result languageIsNull(LanguageIsNull<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (languageIsNull != null) {
      return languageIsNull(this);
    }
    return orElse();
  }
}

abstract class LanguageIsNull<T> implements ValueFailure<T> {
  const factory LanguageIsNull({@required T failedValue}) = _$LanguageIsNull<T>;

  @override
  T get failedValue;
  @override
  $LanguageIsNullCopyWith<T, LanguageIsNull<T>> get copyWith;
}
