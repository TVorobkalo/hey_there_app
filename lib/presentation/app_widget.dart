import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hey_there_app/generated/l10n.dart';
import 'package:hey_there_app/injection.dart';
import 'package:hey_there_app/locale/locale_bloc.dart';
import 'package:hey_there_app/presentation/routes/router.gr.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<LocaleBloc>(),
      child: MaterialApp(
            title: 'Hey there app',
            debugShowCheckedModeBanner: false,
            // locale: state.locale,
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            supportedLocales: S.delegate.supportedLocales,
            builder: ExtendedNavigator.builder<Router>(router: Router()),
            theme: ThemeData(
              primarySwatch: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),

      ),
    );
  }
}
