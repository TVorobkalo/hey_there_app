import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hey_there_app/generated/l10n.dart';
import 'package:hey_there_app/injection.dart';
import 'package:hey_there_app/locale/locale_bloc.dart';
import 'package:hey_there_app/application/page/page_bloc.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    context.bloc<LocaleBloc>().add(LocaleEvent.loadLocales(context));
    return BlocProvider(
      create: (_) => getIt<PageBloc>(),
      child: BlocConsumer<PageBloc, PageState>(
        listener: (context, state) {},
        builder: (context, state) {
          return Scaffold(
            body: InkWell(
              onTap: () {
                context.bloc<PageBloc>().add(const PageEvent.clickDetected());
              },
              child: Container(
                alignment: Alignment.center,
                color: state.color,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      S.of(context).hey_there,
                      style: TextStyle(
                        fontSize: 35,
                      ),
                    ),
                  ],
                ),
              ),
            ), // This trailing comma makes auto-formatting nicer for build methods.
          );
        },
      ),
    );
  }
}
