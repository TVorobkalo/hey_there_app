import 'package:auto_route/auto_route_annotations.dart';
import 'package:hey_there_app/presentation/pages/page.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: MainPage, initial: true),
])
class $Router {}
