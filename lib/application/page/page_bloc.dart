import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'dart:async';
import 'dart:math';

part 'page_bloc.freezed.dart';
part 'page_event.dart';
part 'page_state.dart';

@injectable
class PageBloc extends Bloc<PageEvent, PageState> {
  PageBloc() : super(PageState.initial());

  @override
  Stream<PageState> mapEventToState(PageEvent event) async* {
    yield* event.map(
      clickDetected: (e) async* {
        yield state.copyWith(clickDetected: true, color: _generateColor());
      },
    );
  }

  Color _generateColor() {
    Random random = Random();
    final int hexColor = 0xFF;
    return Color.fromARGB(
      hexColor,
      random.nextInt(hexColor),
      random.nextInt(hexColor),
      random.nextInt(hexColor),
    );
  }
}
