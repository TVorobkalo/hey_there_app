// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'page_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$PageEventTearOff {
  const _$PageEventTearOff();

// ignore: unused_element
  ClickDetected clickDetected() {
    return const ClickDetected();
  }
}

// ignore: unused_element
const $PageEvent = _$PageEventTearOff();

mixin _$PageEvent {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result clickDetected(),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result clickDetected(),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result clickDetected(ClickDetected value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result clickDetected(ClickDetected value),
    @required Result orElse(),
  });
}

abstract class $PageEventCopyWith<$Res> {
  factory $PageEventCopyWith(PageEvent value, $Res Function(PageEvent) then) =
      _$PageEventCopyWithImpl<$Res>;
}

class _$PageEventCopyWithImpl<$Res> implements $PageEventCopyWith<$Res> {
  _$PageEventCopyWithImpl(this._value, this._then);

  final PageEvent _value;
  // ignore: unused_field
  final $Res Function(PageEvent) _then;
}

abstract class $ClickDetectedCopyWith<$Res> {
  factory $ClickDetectedCopyWith(
          ClickDetected value, $Res Function(ClickDetected) then) =
      _$ClickDetectedCopyWithImpl<$Res>;
}

class _$ClickDetectedCopyWithImpl<$Res> extends _$PageEventCopyWithImpl<$Res>
    implements $ClickDetectedCopyWith<$Res> {
  _$ClickDetectedCopyWithImpl(
      ClickDetected _value, $Res Function(ClickDetected) _then)
      : super(_value, (v) => _then(v as ClickDetected));

  @override
  ClickDetected get _value => super._value as ClickDetected;
}

class _$ClickDetected implements ClickDetected {
  const _$ClickDetected();

  @override
  String toString() {
    return 'PageEvent.clickDetected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is ClickDetected);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result clickDetected(),
  }) {
    assert(clickDetected != null);
    return clickDetected();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result clickDetected(),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (clickDetected != null) {
      return clickDetected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result clickDetected(ClickDetected value),
  }) {
    assert(clickDetected != null);
    return clickDetected(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result clickDetected(ClickDetected value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (clickDetected != null) {
      return clickDetected(this);
    }
    return orElse();
  }
}

abstract class ClickDetected implements PageEvent {
  const factory ClickDetected() = _$ClickDetected;
}

class _$PageStateTearOff {
  const _$PageStateTearOff();

// ignore: unused_element
  _PageState call({@required bool clickDetected, @required Color color}) {
    return _PageState(
      clickDetected: clickDetected,
      color: color,
    );
  }
}

// ignore: unused_element
const $PageState = _$PageStateTearOff();

mixin _$PageState {
  bool get clickDetected;
  Color get color;

  $PageStateCopyWith<PageState> get copyWith;
}

abstract class $PageStateCopyWith<$Res> {
  factory $PageStateCopyWith(PageState value, $Res Function(PageState) then) =
      _$PageStateCopyWithImpl<$Res>;
  $Res call({bool clickDetected, Color color});
}

class _$PageStateCopyWithImpl<$Res> implements $PageStateCopyWith<$Res> {
  _$PageStateCopyWithImpl(this._value, this._then);

  final PageState _value;
  // ignore: unused_field
  final $Res Function(PageState) _then;

  @override
  $Res call({
    Object clickDetected = freezed,
    Object color = freezed,
  }) {
    return _then(_value.copyWith(
      clickDetected: clickDetected == freezed
          ? _value.clickDetected
          : clickDetected as bool,
      color: color == freezed ? _value.color : color as Color,
    ));
  }
}

abstract class _$PageStateCopyWith<$Res> implements $PageStateCopyWith<$Res> {
  factory _$PageStateCopyWith(
          _PageState value, $Res Function(_PageState) then) =
      __$PageStateCopyWithImpl<$Res>;
  @override
  $Res call({bool clickDetected, Color color});
}

class __$PageStateCopyWithImpl<$Res> extends _$PageStateCopyWithImpl<$Res>
    implements _$PageStateCopyWith<$Res> {
  __$PageStateCopyWithImpl(_PageState _value, $Res Function(_PageState) _then)
      : super(_value, (v) => _then(v as _PageState));

  @override
  _PageState get _value => super._value as _PageState;

  @override
  $Res call({
    Object clickDetected = freezed,
    Object color = freezed,
  }) {
    return _then(_PageState(
      clickDetected: clickDetected == freezed
          ? _value.clickDetected
          : clickDetected as bool,
      color: color == freezed ? _value.color : color as Color,
    ));
  }
}

class _$_PageState extends _PageState {
  const _$_PageState({@required this.clickDetected, @required this.color})
      : assert(clickDetected != null),
        assert(color != null),
        super._();

  @override
  final bool clickDetected;
  @override
  final Color color;

  @override
  String toString() {
    return 'PageState(clickDetected: $clickDetected, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PageState &&
            (identical(other.clickDetected, clickDetected) ||
                const DeepCollectionEquality()
                    .equals(other.clickDetected, clickDetected)) &&
            (identical(other.color, color) ||
                const DeepCollectionEquality().equals(other.color, color)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(clickDetected) ^
      const DeepCollectionEquality().hash(color);

  @override
  _$PageStateCopyWith<_PageState> get copyWith =>
      __$PageStateCopyWithImpl<_PageState>(this, _$identity);
}

abstract class _PageState extends PageState {
  const _PageState._() : super._();
  const factory _PageState(
      {@required bool clickDetected, @required Color color}) = _$_PageState;

  @override
  bool get clickDetected;
  @override
  Color get color;
  @override
  _$PageStateCopyWith<_PageState> get copyWith;
}
