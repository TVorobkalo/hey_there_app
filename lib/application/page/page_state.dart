part of 'page_bloc.dart';

@freezed
abstract class PageState  with _$PageState{
  const PageState._();

  const factory PageState({
    @required bool clickDetected,
    @required Color color,
  }) = _PageState;

  factory PageState.initial() {
    return PageState(
      clickDetected: false,
      color: Colors.blue,
    );
  }
}