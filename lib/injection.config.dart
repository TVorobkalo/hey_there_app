// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'locale/locale_bloc.dart';
import 'locale/locale_service.dart';
import 'application/page/page_bloc.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  gh.lazySingleton<LocaleBloc>(() => LocaleBloc());
  gh.factory<PageBloc>(() => PageBloc());

  // Eager singletons must be registered in the right order
  gh.singletonAsync<LocaleService>(() => LocaleService.create());
  return get;
}
